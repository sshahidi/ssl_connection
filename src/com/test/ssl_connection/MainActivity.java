

package com.test.ssl_connection;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.xml.datatype.DatatypeFactory;

import protos.smartdata.CommandProtos;
import protos.smartdata.CommandProtos.Command;

import android.R.drawable;
import android.net.SSLCertificateSocketFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.security.KeyChain;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {


	TextView stat_txt;
	private final String TAG="SSl main activity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "On create!");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		////
		//sampleproto();
		stat_txt=(TextView) findViewById(R.id.stat_txt);
		stat_txt.setText("bye world!");


		Log.d(TAG,"connecting");
		new makeConnection().execute("hello!");






	}//on create


	//SSL stuff!
	//@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	void connect(String requestString)
	{

		Log.d(TAG,"connect entry");
		int port=8080;//443;
		String host="128.100.70.218";
		//"KeyPairGenerator" service has algorithms DIFFIEHELLMAN, DSA, RSA available to it, 
		//"SSLContext" has algorithms SSL, TLS, SSLV3, DEFAULT, TLSV1.
		try
		{
			Log.d(TAG,"socket with my own cert.");
			//passphrase for keystore
			char[] keystorePass="123456".toCharArray();

			//load own keystore (MyApp just holds reference to application context)
			//KeyStore keyStore=KeyStore.getInstance("BKS");
			KeyStore keyStore=KeyStore.getInstance("jks");
			//keyStore.load(MyApp.getStaticApplicationContext().getResources().openRawResource(R.raw.keystore),keystorePass);
			keyStore.load(this.getResources().openRawResource(R.raw.client_private),keystorePass);
			Log.d(TAG, "hello");

			//create a factory
			TrustManagerFactory tmf= TrustManagerFactory.getInstance("PKCS1");

			//TrustManagerFactory     trustManagerFactory=TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			tmf.init(keyStore);

			//get context
			SSLContext sslContext=SSLContext.getInstance("TLS");

			//init context
			sslContext.init(
					null,
					tmf.getTrustManagers(), 
					new SecureRandom()
					);

			//create the socket
			SSLSocket socket=(SSLSocket) sslContext.getSocketFactory().createSocket("hostname",443);
			//socket.setKeepAlive(true);
			socket.close();
			//			TrustManagerFactory tmf = TrustManagerFactory.getInstance("PKIX");
			//			KeyStore ks = KeyStore.getInstance("JKS");
			//			FileInputStream in = new FileInputStream("C:/cert/client.enc.key");
			//			ks.load(in, "password".toCharArray());
			//			in.close();
			//			tmf.init(ks);
			//			TrustManager[] tms = tmf.getTrustManagers();
			//			
			//			SSLCertificateSocketFactory csf= (SSLCertificateSocketFactory) SSLCertificateSocketFactory.getDefault(2000);
			//			csf.setTrustManagers(tms);
			//			SSLSocket csocket=(SSLSocket) csf.createSocket(host,port);
			//			csocket.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Log.e(TAG,"ERROR2!"+ e.getMessage());
		}

		try
		{
			/////////////////////////
			SSLSocketFactory sf= (SSLSocketFactory) SSLSocketFactory.getDefault();
			SSLSocket socket=(SSLSocket) sf.createSocket(host, port);
			Writer out = new OutputStreamWriter(socket.getOutputStream());
			// https requires the full URL in the GET line
			//out.write(requestString);
			out.write("GET / HTTP/1.0\\r\\\n");
			out.write("\\r\\n");
			out.flush();


			// read response
			BufferedReader in = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
			int c;
			String msg="";
			while ((c = in.read()) != -1) {
				//System.out.write(c);
				//Log.d(TAG,String.valueOf(c));
				msg+=(char)c;
			}
			Log.d(TAG,msg);
			out.close();
			in.close();
			socket.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
			Log.e(TAG,"ERROR!"+ e.getMessage());
		}

	}



	void connect2()
	{
		Log.d(TAG,"connect2");
		int port=443;
		String host="login.yahoo.com";
		SecureRandom sr = new SecureRandom();
		sr.nextInt();

		try {
			char[] keystorePass="123456".toCharArray();

			KeyStore clientKS = KeyStore.getInstance("BKS");
			Log.d(TAG,"loading key store.");
			clientKS.load(this.getResources().openRawResource(R.raw.bks_keystore),keystorePass);
			//clientKS.load(null);

			//clientKeyStore.setCertificateEntry("clientprivate", cert);
			//KeyStore.PrivateKeyEntry
			Log.d(TAG, "key store loaded");

			KeyStore serverKeyStore = KeyStore.getInstance("BKS");
			//serverKeyStore.load(this.getResources().openRawResource(R.raw.mycert), keystorePass);
			serverKeyStore.load(null);

			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(serverKeyStore);

			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(clientKS, keystorePass);

			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), sr);

			SSLSocketFactory sf = sslContext.getSocketFactory();
			SSLSocket ss = (SSLSocket)sf.createSocket(host, port);
			ss.setNeedClientAuth(true);

			BufferedReader in = new BufferedReader(new InputStreamReader(ss.getInputStream()));

			String line = null;
			while((line = in.readLine()) != null)
			{
				System.out.println(line);
			}
			in.close();
			ss.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.e(TAG,"this problem happened: "+ e.getMessage());
		}
	}


	void connect3()
	{
		char[] keystorePass="123456".toCharArray();
		Log.d(TAG,"Connect3");
		// Load CAs from an InputStream (could be a resource or ByteArrayInputStream or ...)
		Log.d(TAG,"loading CA cert to key store.");
		KeyStore keyStore;
		try {
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(null, keystorePass);



			CertificateFactory cf = null;
			Certificate ca=null;
			try {
				cf = CertificateFactory.getInstance("X.509");
			} catch (CertificateException e) {
				Log.e(TAG,"exception in cf: "+ e.getMessage());
			}

			try {
				//caInput = new BufferedInputStream(new FileInputStream("load-der.crt"));
				ca = cf.generateCertificate(this.getResources().openRawResource(R.raw.ca));
				//caInput.close();

				// Create a KeyStore containing our trusted CAs
				//String keyStoreType = KeyStore.getDefaultType();
				//KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
				//keyStore.load(null, null);
				keyStore.setCertificateEntry("ca", ca); 
				Log.d(TAG,"keystore loaded successfully.");
				Log.d(TAG,"type: "+keyStore.getType());
				Log.d(TAG,"alias: "+keyStore.getCertificateAlias(ca));
				Log.d(TAG,"certificate: "+keyStore.getCertificate("ca").getPublicKey().toString());

			} catch (Exception e) {
				Log.e(TAG,"exception in ca: "+ e.getMessage()); 

			}

			///////////////////////////////////////////////////////////////
			////////////////client
			Log.d(TAG,"loading client cert to key store.");


			cf = null;
			Certificate client=null;
			try {
				cf = CertificateFactory.getInstance("X.509");
			} catch (CertificateException e) {
				Log.e(TAG,"exception in cf: "+ e.getMessage());
			}

			try {
				//caInput = new BufferedInputStream(new FileInputStream("load-der.crt"));
				client = cf.generateCertificate(this.getResources().openRawResource(R.raw.client));
				//caInput.close();

				// Create a KeyStore containing our trusted CAs
				//String keyStoreType = KeyStore.getDefaultType();
				//KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
				//keyStore.load(null, null);
				keyStore.setCertificateEntry("client", client); 
				Log.d(TAG,"keystore loaded successfully.");
				Log.d(TAG,"type: "+keyStore.getType());
				Log.d(TAG,"alias: "+keyStore.getCertificateAlias(client));
				Log.d(TAG,"certificate: "+keyStore.getCertificate("client").getPublicKey().toString());

				// add a client certificate with private key to the keystore.
				InputStream buffer=this.getResources().openRawResource(R.raw.client_key);
				int c;
				String pk="";
				while ((c = buffer.read()) != -1) {
					pk+=(char)c;
				}
				buffer.close();

				String[] tokens = pk.split("-----BEGIN RSA PRIVATE KEY-----");
				tokens = tokens[1].split("-----END RSA PRIVATE KEY-----");
				pk=tokens[0];
				tokens=pk.split("\n");
				String temp="";
				for(int i=0;i<tokens.length;i++)
					temp+=tokens[i];
				pk=temp;
				//byte[] pk_byts=Base64.encode(pk.getBytes(), Base64.DEFAULT);

				byte[] pk_byts=pk.getBytes();
				//PrivateKey key = (PrivateKey)pk_byts;      

				Log.d(TAG,"private key: "+ pk);
				keyStore.setKeyEntry("client", pk_byts, new Certificate[] {ca});

				Log.d(TAG,"private key loaded.");
				//Log.d(TAG,keyStore.getKey("client", "123456".toCharArray()).toString());

			} catch (Exception e) {
				Log.e(TAG,"exception in client: "+ e.getMessage()); 

			}

			///////////////////////////////////////////////////////////////////////////////////////
			// Load server from an InputStream (could be a resource or ByteArrayInputStream or ...)
			Log.d(TAG,"loading server cert to key store.");
			cf = null;
			Certificate server=null;
			try {
				cf = CertificateFactory.getInstance("X.509");
			} catch (CertificateException e) {
				Log.e(TAG,"exception in cf: "+ e.getMessage());
			}

			try {
				//caInput = new BufferedInputStream(new FileInputStream("load-der.crt"));
				server = cf.generateCertificate(this.getResources().openRawResource(R.raw.server));
				//caInput.close();

				// Create a KeyStore containing our trusted CAs
				//String keyStoreType = KeyStore.getDefaultType();
				//KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
				//keyStore.load(null, null);
				keyStore.setCertificateEntry("server", server); 
				Log.d(TAG,"keystore loaded successfully.");
				Log.d(TAG,"type: "+keyStore.getType());
				Log.d(TAG,"alias: "+keyStore.getCertificateAlias(server));
				Log.d(TAG,"certificate: "+keyStore.getCertificate("server").getPublicKey().toString());

			} catch (Exception e) {
				Log.e(TAG,"exception in server: "+ e.getMessage()); 

			}



			/////////////////////////////////////
			////the connection part.
			int port=8008;
			//String host="128.100.70.218";
			String host="localhost";
			SecureRandom sr = new SecureRandom();
			sr.nextInt();

			try {


				//////KeyStore clientKS = KeyStore.getInstance("BKS");
				//////Log.d(TAG,"loading key store.");
				//clientKeyStore.load(this.getResources().openRawResource(R.raw.mycert),keystorePass);
				///////clientKS.load(null);
				//Certificate cert=new Ce
				//clientKeyStore.setCertificateEntry("clientprivate", cert);
				//KeyStore.PrivateKeyEntry
				//////Log.d(TAG, "key store loaded");

				/////KeyStore serverKeyStore = KeyStore.getInstance("BKS");
				//serverKeyStore.load(this.getResources().openRawResource(R.raw.mycert), keystorePass);
				//////serverKeyStore.load(null);

				TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				tmf.init(keyStore);

				KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
				kmf.init(keyStore, keystorePass);

				SSLContext sslContext = SSLContext.getInstance("TLS");
				sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), sr);

				SSLSocketFactory sf = sslContext.getSocketFactory();
				
				SSLSocket ss = (SSLSocket)sf.createSocket(host, port);
				ss.setNeedClientAuth(true);

				BufferedReader in = new BufferedReader(new InputStreamReader(ss.getInputStream()));

				String line = null;
				while((line = in.readLine()) != null)
				{
					System.out.println(line);
				}
				in.close();
				ss.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				Log.e(TAG,"this problem happened: "+ e.getMessage());
			}
		} catch (Exception e) {
			Log.e(TAG,"alaki!");
		}


	}


/**
 * the 4-th method! hopefully this works!
 */
	void connect4()
	{
		Log.d(TAG,"connect4");

		////////////////client
		Log.d(TAG,"loading client cert to key store.");
		KeyStore keyStore=null;
		KeyStore trustStore=null;
		char[] keyPass="123456".toCharArray();
		
		
		// Load CAs from an InputStream (could be a resource or ByteArrayInputStream or ...)
		Log.d(TAG,"loading CA cert to key store.");
		CertificateFactory cf = null;
		try {
			cf = CertificateFactory.getInstance("X.509");
		} catch (CertificateException e) {
			Log.e(TAG,"exception in cf: "+ e.getMessage());
		}

		try {
			String alias="client";
			//loading the public key
			Certificate client = cf.generateCertificate(this.getResources().openRawResource(R.raw.client));

			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(null, keyPass);
			keyStore.setCertificateEntry(alias, client); 
			Log.d(TAG,"keystore loaded successfully.");
			Log.d(TAG,"type: "+keyStore.getType());
			Log.d(TAG,"alias: "+keyStore.getCertificateAlias(client));
			Log.d(TAG,"certificate: "+keyStore.getCertificate(alias).getPublicKey().toString());

			// loading the private key.
			InputStream fl = this.getResources().openRawResource(R.raw.der_client_key);
			byte[] key = new byte[fl.available()];
			fl.read ( key, 0, fl.available() );
			fl.close();
			KeyFactory kf = KeyFactory.getInstance("RSA");
			PKCS8EncodedKeySpec keysp = new PKCS8EncodedKeySpec ( key );
			PrivateKey ff = kf.generatePrivate (keysp);

			// storing keystore
			keyStore.setKeyEntry(alias, ff, keyPass, new Certificate[] {client} );
			///////////

			Log.d(TAG,"private key loaded.");
			Log.d(TAG,keyStore.getKey("client", keyPass).toString());

		} catch (Exception e) {
			Log.e(TAG,"exception in client: "+ e.getMessage()); 
		}
		
		
		//loading server and ca certificates.
		try
		{
			String alias="server";
			//loading the public key
			Certificate server = cf.generateCertificate(this.getResources().openRawResource(R.raw.server));

			trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, keyPass);
			trustStore.setCertificateEntry(alias, server); 
			Log.d(TAG,"server truststore loaded successfully.");
			Log.d(TAG,"type: "+trustStore.getType());
			Log.d(TAG,"alias: "+trustStore.getCertificateAlias(server));
			//Log.d(TAG,"certificate: "+trustStore.getCertificate(alias).getPublicKey().toString());
			
			alias="ca";
			Certificate ca = cf.generateCertificate(this.getResources().openRawResource(R.raw.ca));
			trustStore.setCertificateEntry(alias, ca); 
			Log.d(TAG,"ca truststore loaded successfully.");
			Log.d(TAG,"type: "+trustStore.getType());
			Log.d(TAG,"alias: "+trustStore.getCertificateAlias(server));
			//Log.d(TAG,"certificate: "+trustStore.getCertificate(alias).getPublicKey().toString());
			
		}
		catch(Exception e)
		{
			Log.e(TAG,"exception in server certs: "+ e.getMessage());
		}
		
		
		///setting up the ssl context and openning an ssl socket.
		try
		{
			int port=8008;
			String host="128.100.207.94"; //"128.100.70.208"; //"login.yahoo.com";
			
			SecureRandom sr = new SecureRandom();
			sr.nextInt();
			
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(trustStore);
			Log.d(TAG,"trust manager initialized.");

			KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			kmf.init(keyStore, keyPass);
			Log.d(TAG,"key manager initialized.");

			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), sr);
			Log.d(TAG,"SSL context initialized.");
			
			SSLSocketFactory sf = sslContext.getSocketFactory();
			SSLSocket ss = (SSLSocket)sf.createSocket(host, port);
			Log.d(TAG,"Socket opened successfully. authenticating...");
			//ss.setNeedClientAuth(true); // this is probably not required in client mode.
			ss.setUseClientMode(true); //not sure if this is required
			if(ss.isConnected())
				Log.d(TAG,"connected.");
			
			//genarting sample command:
			CommandProtos.Command command=CommandProtos.Command.newBuilder()
					.setUsername("alaki")
					.setCommand("dolaki!")
					.build();
			
		
			//writing to socket:
			Writer out = new OutputStreamWriter(ss.getOutputStream());
			// https requires the full URL in the GET line
			//out.write(requestString);
			String stuff=new String(command.toByteArray());
			out.write(stuff);
			//out.write("\\r\\n");
			out.flush();
			Log.d(TAG,"wrote some stuff!");
			//reading from socket:
			BufferedReader in = new BufferedReader(new InputStreamReader(ss.getInputStream()));

			String line = null;
			while((line = in.readLine()) != null)
			{
				Log.d(TAG,"line");
				System.out.println(line);
			}
			in.close();
			ss.close();
		}
		catch(Exception e)
		{
			Log.e(TAG,"exception in ssl connection: "+ e.getMessage());
		}


	}//end of connect4()

	
	void sampleproto()
	{
		CommandProtos.Command command=CommandProtos.Command.newBuilder()
				.setUsername("alaki")
				.setCommand("dolaki!")
				.build();
	}
	


	class makeConnection extends AsyncTask<String, Void, Boolean> {

		private Exception exception;

		protected Boolean doInBackground(String... urls) {
			try {
				Log.d(TAG,urls[0]);
				//connect(urls[0]);
				//connect("alai");
				connect4();

				return true;
			} catch (Exception e) {
				return false;
			}

		}

		protected void onPostExecute() {
			// TODO: check this.exception 
			// TODO: do something with the feed
		}
	}

	/////////////////////////////////////////////

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
