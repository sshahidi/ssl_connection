// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: feedback.proto

package protos.smartdata;

public final class FeedbackProtos {
  private FeedbackProtos() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface FeedbackOrBuilder
      extends com.google.protobuf.MessageOrBuilder {
    
    // optional float satisfaction = 1;
    boolean hasSatisfaction();
    float getSatisfaction();
    
    // optional .clark_pb.Feedback.AttemptResult attemptResult = 2 [default = SUCCESS_NO_FEEDBACK];
    boolean hasAttemptResult();
    protos.smartdata.FeedbackProtos.Feedback.AttemptResult getAttemptResult();
    
    // optional bool action_feedback = 3;
    boolean hasActionFeedback();
    boolean getActionFeedback();
    
    // optional bool itemDescription_feedback = 4;
    boolean hasItemDescriptionFeedback();
    boolean getItemDescriptionFeedback();
    
    // optional bool item_feedback = 5;
    boolean hasItemFeedback();
    boolean getItemFeedback();
    
    // optional bool deliverTo_feedback = 6;
    boolean hasDeliverToFeedback();
    boolean getDeliverToFeedback();
    
    // optional bool price_feedback = 7;
    boolean hasPriceFeedback();
    boolean getPriceFeedback();
    
    // optional bool privacy_feedback = 8;
    boolean hasPrivacyFeedback();
    boolean getPrivacyFeedback();
    
    // optional bool time_feedback = 9;
    boolean hasTimeFeedback();
    boolean getTimeFeedback();
    
    // optional bool vendor_feedback = 10;
    boolean hasVendorFeedback();
    boolean getVendorFeedback();
    
    // required string hash_code = 11;
    boolean hasHashCode();
    String getHashCode();
  }
  public static final class Feedback extends
      com.google.protobuf.GeneratedMessage
      implements FeedbackOrBuilder {
    // Use Feedback.newBuilder() to construct.
    private Feedback(Builder builder) {
      super(builder);
    }
    private Feedback(boolean noInit) {}
    
    private static final Feedback defaultInstance;
    public static Feedback getDefaultInstance() {
      return defaultInstance;
    }
    
    public Feedback getDefaultInstanceForType() {
      return defaultInstance;
    }
    
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return protos.smartdata.FeedbackProtos.internal_static_clark_pb_Feedback_descriptor;
    }
    
    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return protos.smartdata.FeedbackProtos.internal_static_clark_pb_Feedback_fieldAccessorTable;
    }
    
    public enum AttemptResult
        implements com.google.protobuf.ProtocolMessageEnum {
      SUCCESS(0, 0),
      SUCCESS_NO_FEEDBACK(1, 1),
      SUCCESS_NO_DETAIL_FEEDBACK(2, 2),
      FAILURE_NO_RESPONE(3, 3),
      ;
      
      public static final int SUCCESS_VALUE = 0;
      public static final int SUCCESS_NO_FEEDBACK_VALUE = 1;
      public static final int SUCCESS_NO_DETAIL_FEEDBACK_VALUE = 2;
      public static final int FAILURE_NO_RESPONE_VALUE = 3;
      
      
      public final int getNumber() { return value; }
      
      public static AttemptResult valueOf(int value) {
        switch (value) {
          case 0: return SUCCESS;
          case 1: return SUCCESS_NO_FEEDBACK;
          case 2: return SUCCESS_NO_DETAIL_FEEDBACK;
          case 3: return FAILURE_NO_RESPONE;
          default: return null;
        }
      }
      
      public static com.google.protobuf.Internal.EnumLiteMap<AttemptResult>
          internalGetValueMap() {
        return internalValueMap;
      }
      private static com.google.protobuf.Internal.EnumLiteMap<AttemptResult>
          internalValueMap =
            new com.google.protobuf.Internal.EnumLiteMap<AttemptResult>() {
              public AttemptResult findValueByNumber(int number) {
                return AttemptResult.valueOf(number);
              }
            };
      
      public final com.google.protobuf.Descriptors.EnumValueDescriptor
          getValueDescriptor() {
        return getDescriptor().getValues().get(index);
      }
      public final com.google.protobuf.Descriptors.EnumDescriptor
          getDescriptorForType() {
        return getDescriptor();
      }
      public static final com.google.protobuf.Descriptors.EnumDescriptor
          getDescriptor() {
        return protos.smartdata.FeedbackProtos.Feedback.getDescriptor().getEnumTypes().get(0);
      }
      
      private static final AttemptResult[] VALUES = {
        SUCCESS, SUCCESS_NO_FEEDBACK, SUCCESS_NO_DETAIL_FEEDBACK, FAILURE_NO_RESPONE, 
      };
      
      public static AttemptResult valueOf(
          com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
        if (desc.getType() != getDescriptor()) {
          throw new java.lang.IllegalArgumentException(
            "EnumValueDescriptor is not for this type.");
        }
        return VALUES[desc.getIndex()];
      }
      
      private final int index;
      private final int value;
      
      private AttemptResult(int index, int value) {
        this.index = index;
        this.value = value;
      }
      
      // @@protoc_insertion_point(enum_scope:clark_pb.Feedback.AttemptResult)
    }
    
    private int bitField0_;
    // optional float satisfaction = 1;
    public static final int SATISFACTION_FIELD_NUMBER = 1;
    private float satisfaction_;
    public boolean hasSatisfaction() {
      return ((bitField0_ & 0x00000001) == 0x00000001);
    }
    public float getSatisfaction() {
      return satisfaction_;
    }
    
    // optional .clark_pb.Feedback.AttemptResult attemptResult = 2 [default = SUCCESS_NO_FEEDBACK];
    public static final int ATTEMPTRESULT_FIELD_NUMBER = 2;
    private protos.smartdata.FeedbackProtos.Feedback.AttemptResult attemptResult_;
    public boolean hasAttemptResult() {
      return ((bitField0_ & 0x00000002) == 0x00000002);
    }
    public protos.smartdata.FeedbackProtos.Feedback.AttemptResult getAttemptResult() {
      return attemptResult_;
    }
    
    // optional bool action_feedback = 3;
    public static final int ACTION_FEEDBACK_FIELD_NUMBER = 3;
    private boolean actionFeedback_;
    public boolean hasActionFeedback() {
      return ((bitField0_ & 0x00000004) == 0x00000004);
    }
    public boolean getActionFeedback() {
      return actionFeedback_;
    }
    
    // optional bool itemDescription_feedback = 4;
    public static final int ITEMDESCRIPTION_FEEDBACK_FIELD_NUMBER = 4;
    private boolean itemDescriptionFeedback_;
    public boolean hasItemDescriptionFeedback() {
      return ((bitField0_ & 0x00000008) == 0x00000008);
    }
    public boolean getItemDescriptionFeedback() {
      return itemDescriptionFeedback_;
    }
    
    // optional bool item_feedback = 5;
    public static final int ITEM_FEEDBACK_FIELD_NUMBER = 5;
    private boolean itemFeedback_;
    public boolean hasItemFeedback() {
      return ((bitField0_ & 0x00000010) == 0x00000010);
    }
    public boolean getItemFeedback() {
      return itemFeedback_;
    }
    
    // optional bool deliverTo_feedback = 6;
    public static final int DELIVERTO_FEEDBACK_FIELD_NUMBER = 6;
    private boolean deliverToFeedback_;
    public boolean hasDeliverToFeedback() {
      return ((bitField0_ & 0x00000020) == 0x00000020);
    }
    public boolean getDeliverToFeedback() {
      return deliverToFeedback_;
    }
    
    // optional bool price_feedback = 7;
    public static final int PRICE_FEEDBACK_FIELD_NUMBER = 7;
    private boolean priceFeedback_;
    public boolean hasPriceFeedback() {
      return ((bitField0_ & 0x00000040) == 0x00000040);
    }
    public boolean getPriceFeedback() {
      return priceFeedback_;
    }
    
    // optional bool privacy_feedback = 8;
    public static final int PRIVACY_FEEDBACK_FIELD_NUMBER = 8;
    private boolean privacyFeedback_;
    public boolean hasPrivacyFeedback() {
      return ((bitField0_ & 0x00000080) == 0x00000080);
    }
    public boolean getPrivacyFeedback() {
      return privacyFeedback_;
    }
    
    // optional bool time_feedback = 9;
    public static final int TIME_FEEDBACK_FIELD_NUMBER = 9;
    private boolean timeFeedback_;
    public boolean hasTimeFeedback() {
      return ((bitField0_ & 0x00000100) == 0x00000100);
    }
    public boolean getTimeFeedback() {
      return timeFeedback_;
    }
    
    // optional bool vendor_feedback = 10;
    public static final int VENDOR_FEEDBACK_FIELD_NUMBER = 10;
    private boolean vendorFeedback_;
    public boolean hasVendorFeedback() {
      return ((bitField0_ & 0x00000200) == 0x00000200);
    }
    public boolean getVendorFeedback() {
      return vendorFeedback_;
    }
    
    // required string hash_code = 11;
    public static final int HASH_CODE_FIELD_NUMBER = 11;
    private java.lang.Object hashCode_;
    public boolean hasHashCode() {
      return ((bitField0_ & 0x00000400) == 0x00000400);
    }
    public String getHashCode() {
      java.lang.Object ref = hashCode_;
      if (ref instanceof String) {
        return (String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        String s = bs.toStringUtf8();
        if (com.google.protobuf.Internal.isValidUtf8(bs)) {
          hashCode_ = s;
        }
        return s;
      }
    }
    private com.google.protobuf.ByteString getHashCodeBytes() {
      java.lang.Object ref = hashCode_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8((String) ref);
        hashCode_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    
    private void initFields() {
      satisfaction_ = 0F;
      attemptResult_ = protos.smartdata.FeedbackProtos.Feedback.AttemptResult.SUCCESS_NO_FEEDBACK;
      actionFeedback_ = false;
      itemDescriptionFeedback_ = false;
      itemFeedback_ = false;
      deliverToFeedback_ = false;
      priceFeedback_ = false;
      privacyFeedback_ = false;
      timeFeedback_ = false;
      vendorFeedback_ = false;
      hashCode_ = "";
    }
    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized != -1) return isInitialized == 1;
      
      if (!hasHashCode()) {
        memoizedIsInitialized = 0;
        return false;
      }
      memoizedIsInitialized = 1;
      return true;
    }
    
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        output.writeFloat(1, satisfaction_);
      }
      if (((bitField0_ & 0x00000002) == 0x00000002)) {
        output.writeEnum(2, attemptResult_.getNumber());
      }
      if (((bitField0_ & 0x00000004) == 0x00000004)) {
        output.writeBool(3, actionFeedback_);
      }
      if (((bitField0_ & 0x00000008) == 0x00000008)) {
        output.writeBool(4, itemDescriptionFeedback_);
      }
      if (((bitField0_ & 0x00000010) == 0x00000010)) {
        output.writeBool(5, itemFeedback_);
      }
      if (((bitField0_ & 0x00000020) == 0x00000020)) {
        output.writeBool(6, deliverToFeedback_);
      }
      if (((bitField0_ & 0x00000040) == 0x00000040)) {
        output.writeBool(7, priceFeedback_);
      }
      if (((bitField0_ & 0x00000080) == 0x00000080)) {
        output.writeBool(8, privacyFeedback_);
      }
      if (((bitField0_ & 0x00000100) == 0x00000100)) {
        output.writeBool(9, timeFeedback_);
      }
      if (((bitField0_ & 0x00000200) == 0x00000200)) {
        output.writeBool(10, vendorFeedback_);
      }
      if (((bitField0_ & 0x00000400) == 0x00000400)) {
        output.writeBytes(11, getHashCodeBytes());
      }
      getUnknownFields().writeTo(output);
    }
    
    private int memoizedSerializedSize = -1;
    public int getSerializedSize() {
      int size = memoizedSerializedSize;
      if (size != -1) return size;
    
      size = 0;
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        size += com.google.protobuf.CodedOutputStream
          .computeFloatSize(1, satisfaction_);
      }
      if (((bitField0_ & 0x00000002) == 0x00000002)) {
        size += com.google.protobuf.CodedOutputStream
          .computeEnumSize(2, attemptResult_.getNumber());
      }
      if (((bitField0_ & 0x00000004) == 0x00000004)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(3, actionFeedback_);
      }
      if (((bitField0_ & 0x00000008) == 0x00000008)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(4, itemDescriptionFeedback_);
      }
      if (((bitField0_ & 0x00000010) == 0x00000010)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(5, itemFeedback_);
      }
      if (((bitField0_ & 0x00000020) == 0x00000020)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(6, deliverToFeedback_);
      }
      if (((bitField0_ & 0x00000040) == 0x00000040)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(7, priceFeedback_);
      }
      if (((bitField0_ & 0x00000080) == 0x00000080)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(8, privacyFeedback_);
      }
      if (((bitField0_ & 0x00000100) == 0x00000100)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(9, timeFeedback_);
      }
      if (((bitField0_ & 0x00000200) == 0x00000200)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBoolSize(10, vendorFeedback_);
      }
      if (((bitField0_ & 0x00000400) == 0x00000400)) {
        size += com.google.protobuf.CodedOutputStream
          .computeBytesSize(11, getHashCodeBytes());
      }
      size += getUnknownFields().getSerializedSize();
      memoizedSerializedSize = size;
      return size;
    }
    
    private static final long serialVersionUID = 0L;
    @java.lang.Override
    protected java.lang.Object writeReplace()
        throws java.io.ObjectStreamException {
      return super.writeReplace();
    }
    
    public static protos.smartdata.FeedbackProtos.Feedback parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return newBuilder().mergeFrom(data).buildParsed();
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return newBuilder().mergeFrom(data, extensionRegistry)
               .buildParsed();
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return newBuilder().mergeFrom(data).buildParsed();
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return newBuilder().mergeFrom(data, extensionRegistry)
               .buildParsed();
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return newBuilder().mergeFrom(input).buildParsed();
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return newBuilder().mergeFrom(input, extensionRegistry)
               .buildParsed();
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      Builder builder = newBuilder();
      if (builder.mergeDelimitedFrom(input)) {
        return builder.buildParsed();
      } else {
        return null;
      }
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      Builder builder = newBuilder();
      if (builder.mergeDelimitedFrom(input, extensionRegistry)) {
        return builder.buildParsed();
      } else {
        return null;
      }
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return newBuilder().mergeFrom(input).buildParsed();
    }
    public static protos.smartdata.FeedbackProtos.Feedback parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return newBuilder().mergeFrom(input, extensionRegistry)
               .buildParsed();
    }
    
    public static Builder newBuilder() { return Builder.create(); }
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder(protos.smartdata.FeedbackProtos.Feedback prototype) {
      return newBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() { return newBuilder(this); }
    
    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder>
       implements protos.smartdata.FeedbackProtos.FeedbackOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return protos.smartdata.FeedbackProtos.internal_static_clark_pb_Feedback_descriptor;
      }
      
      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return protos.smartdata.FeedbackProtos.internal_static_clark_pb_Feedback_fieldAccessorTable;
      }
      
      // Construct using protos.smartdata.FeedbackProtos.Feedback.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }
      
      private Builder(BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      private static Builder create() {
        return new Builder();
      }
      
      public Builder clear() {
        super.clear();
        satisfaction_ = 0F;
        bitField0_ = (bitField0_ & ~0x00000001);
        attemptResult_ = protos.smartdata.FeedbackProtos.Feedback.AttemptResult.SUCCESS_NO_FEEDBACK;
        bitField0_ = (bitField0_ & ~0x00000002);
        actionFeedback_ = false;
        bitField0_ = (bitField0_ & ~0x00000004);
        itemDescriptionFeedback_ = false;
        bitField0_ = (bitField0_ & ~0x00000008);
        itemFeedback_ = false;
        bitField0_ = (bitField0_ & ~0x00000010);
        deliverToFeedback_ = false;
        bitField0_ = (bitField0_ & ~0x00000020);
        priceFeedback_ = false;
        bitField0_ = (bitField0_ & ~0x00000040);
        privacyFeedback_ = false;
        bitField0_ = (bitField0_ & ~0x00000080);
        timeFeedback_ = false;
        bitField0_ = (bitField0_ & ~0x00000100);
        vendorFeedback_ = false;
        bitField0_ = (bitField0_ & ~0x00000200);
        hashCode_ = "";
        bitField0_ = (bitField0_ & ~0x00000400);
        return this;
      }
      
      public Builder clone() {
        return create().mergeFrom(buildPartial());
      }
      
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return protos.smartdata.FeedbackProtos.Feedback.getDescriptor();
      }
      
      public protos.smartdata.FeedbackProtos.Feedback getDefaultInstanceForType() {
        return protos.smartdata.FeedbackProtos.Feedback.getDefaultInstance();
      }
      
      public protos.smartdata.FeedbackProtos.Feedback build() {
        protos.smartdata.FeedbackProtos.Feedback result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }
      
      private protos.smartdata.FeedbackProtos.Feedback buildParsed()
          throws com.google.protobuf.InvalidProtocolBufferException {
        protos.smartdata.FeedbackProtos.Feedback result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(
            result).asInvalidProtocolBufferException();
        }
        return result;
      }
      
      public protos.smartdata.FeedbackProtos.Feedback buildPartial() {
        protos.smartdata.FeedbackProtos.Feedback result = new protos.smartdata.FeedbackProtos.Feedback(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        if (((from_bitField0_ & 0x00000001) == 0x00000001)) {
          to_bitField0_ |= 0x00000001;
        }
        result.satisfaction_ = satisfaction_;
        if (((from_bitField0_ & 0x00000002) == 0x00000002)) {
          to_bitField0_ |= 0x00000002;
        }
        result.attemptResult_ = attemptResult_;
        if (((from_bitField0_ & 0x00000004) == 0x00000004)) {
          to_bitField0_ |= 0x00000004;
        }
        result.actionFeedback_ = actionFeedback_;
        if (((from_bitField0_ & 0x00000008) == 0x00000008)) {
          to_bitField0_ |= 0x00000008;
        }
        result.itemDescriptionFeedback_ = itemDescriptionFeedback_;
        if (((from_bitField0_ & 0x00000010) == 0x00000010)) {
          to_bitField0_ |= 0x00000010;
        }
        result.itemFeedback_ = itemFeedback_;
        if (((from_bitField0_ & 0x00000020) == 0x00000020)) {
          to_bitField0_ |= 0x00000020;
        }
        result.deliverToFeedback_ = deliverToFeedback_;
        if (((from_bitField0_ & 0x00000040) == 0x00000040)) {
          to_bitField0_ |= 0x00000040;
        }
        result.priceFeedback_ = priceFeedback_;
        if (((from_bitField0_ & 0x00000080) == 0x00000080)) {
          to_bitField0_ |= 0x00000080;
        }
        result.privacyFeedback_ = privacyFeedback_;
        if (((from_bitField0_ & 0x00000100) == 0x00000100)) {
          to_bitField0_ |= 0x00000100;
        }
        result.timeFeedback_ = timeFeedback_;
        if (((from_bitField0_ & 0x00000200) == 0x00000200)) {
          to_bitField0_ |= 0x00000200;
        }
        result.vendorFeedback_ = vendorFeedback_;
        if (((from_bitField0_ & 0x00000400) == 0x00000400)) {
          to_bitField0_ |= 0x00000400;
        }
        result.hashCode_ = hashCode_;
        result.bitField0_ = to_bitField0_;
        onBuilt();
        return result;
      }
      
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof protos.smartdata.FeedbackProtos.Feedback) {
          return mergeFrom((protos.smartdata.FeedbackProtos.Feedback)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }
      
      public Builder mergeFrom(protos.smartdata.FeedbackProtos.Feedback other) {
        if (other == protos.smartdata.FeedbackProtos.Feedback.getDefaultInstance()) return this;
        if (other.hasSatisfaction()) {
          setSatisfaction(other.getSatisfaction());
        }
        if (other.hasAttemptResult()) {
          setAttemptResult(other.getAttemptResult());
        }
        if (other.hasActionFeedback()) {
          setActionFeedback(other.getActionFeedback());
        }
        if (other.hasItemDescriptionFeedback()) {
          setItemDescriptionFeedback(other.getItemDescriptionFeedback());
        }
        if (other.hasItemFeedback()) {
          setItemFeedback(other.getItemFeedback());
        }
        if (other.hasDeliverToFeedback()) {
          setDeliverToFeedback(other.getDeliverToFeedback());
        }
        if (other.hasPriceFeedback()) {
          setPriceFeedback(other.getPriceFeedback());
        }
        if (other.hasPrivacyFeedback()) {
          setPrivacyFeedback(other.getPrivacyFeedback());
        }
        if (other.hasTimeFeedback()) {
          setTimeFeedback(other.getTimeFeedback());
        }
        if (other.hasVendorFeedback()) {
          setVendorFeedback(other.getVendorFeedback());
        }
        if (other.hasHashCode()) {
          setHashCode(other.getHashCode());
        }
        this.mergeUnknownFields(other.getUnknownFields());
        return this;
      }
      
      public final boolean isInitialized() {
        if (!hasHashCode()) {
          
          return false;
        }
        return true;
      }
      
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder(
            this.getUnknownFields());
        while (true) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              this.setUnknownFields(unknownFields.build());
              onChanged();
              return this;
            default: {
              if (!parseUnknownField(input, unknownFields,
                                     extensionRegistry, tag)) {
                this.setUnknownFields(unknownFields.build());
                onChanged();
                return this;
              }
              break;
            }
            case 13: {
              bitField0_ |= 0x00000001;
              satisfaction_ = input.readFloat();
              break;
            }
            case 16: {
              int rawValue = input.readEnum();
              protos.smartdata.FeedbackProtos.Feedback.AttemptResult value = protos.smartdata.FeedbackProtos.Feedback.AttemptResult.valueOf(rawValue);
              if (value == null) {
                unknownFields.mergeVarintField(2, rawValue);
              } else {
                bitField0_ |= 0x00000002;
                attemptResult_ = value;
              }
              break;
            }
            case 24: {
              bitField0_ |= 0x00000004;
              actionFeedback_ = input.readBool();
              break;
            }
            case 32: {
              bitField0_ |= 0x00000008;
              itemDescriptionFeedback_ = input.readBool();
              break;
            }
            case 40: {
              bitField0_ |= 0x00000010;
              itemFeedback_ = input.readBool();
              break;
            }
            case 48: {
              bitField0_ |= 0x00000020;
              deliverToFeedback_ = input.readBool();
              break;
            }
            case 56: {
              bitField0_ |= 0x00000040;
              priceFeedback_ = input.readBool();
              break;
            }
            case 64: {
              bitField0_ |= 0x00000080;
              privacyFeedback_ = input.readBool();
              break;
            }
            case 72: {
              bitField0_ |= 0x00000100;
              timeFeedback_ = input.readBool();
              break;
            }
            case 80: {
              bitField0_ |= 0x00000200;
              vendorFeedback_ = input.readBool();
              break;
            }
            case 90: {
              bitField0_ |= 0x00000400;
              hashCode_ = input.readBytes();
              break;
            }
          }
        }
      }
      
      private int bitField0_;
      
      // optional float satisfaction = 1;
      private float satisfaction_ ;
      public boolean hasSatisfaction() {
        return ((bitField0_ & 0x00000001) == 0x00000001);
      }
      public float getSatisfaction() {
        return satisfaction_;
      }
      public Builder setSatisfaction(float value) {
        bitField0_ |= 0x00000001;
        satisfaction_ = value;
        onChanged();
        return this;
      }
      public Builder clearSatisfaction() {
        bitField0_ = (bitField0_ & ~0x00000001);
        satisfaction_ = 0F;
        onChanged();
        return this;
      }
      
      // optional .clark_pb.Feedback.AttemptResult attemptResult = 2 [default = SUCCESS_NO_FEEDBACK];
      private protos.smartdata.FeedbackProtos.Feedback.AttemptResult attemptResult_ = protos.smartdata.FeedbackProtos.Feedback.AttemptResult.SUCCESS_NO_FEEDBACK;
      public boolean hasAttemptResult() {
        return ((bitField0_ & 0x00000002) == 0x00000002);
      }
      public protos.smartdata.FeedbackProtos.Feedback.AttemptResult getAttemptResult() {
        return attemptResult_;
      }
      public Builder setAttemptResult(protos.smartdata.FeedbackProtos.Feedback.AttemptResult value) {
        if (value == null) {
          throw new NullPointerException();
        }
        bitField0_ |= 0x00000002;
        attemptResult_ = value;
        onChanged();
        return this;
      }
      public Builder clearAttemptResult() {
        bitField0_ = (bitField0_ & ~0x00000002);
        attemptResult_ = protos.smartdata.FeedbackProtos.Feedback.AttemptResult.SUCCESS_NO_FEEDBACK;
        onChanged();
        return this;
      }
      
      // optional bool action_feedback = 3;
      private boolean actionFeedback_ ;
      public boolean hasActionFeedback() {
        return ((bitField0_ & 0x00000004) == 0x00000004);
      }
      public boolean getActionFeedback() {
        return actionFeedback_;
      }
      public Builder setActionFeedback(boolean value) {
        bitField0_ |= 0x00000004;
        actionFeedback_ = value;
        onChanged();
        return this;
      }
      public Builder clearActionFeedback() {
        bitField0_ = (bitField0_ & ~0x00000004);
        actionFeedback_ = false;
        onChanged();
        return this;
      }
      
      // optional bool itemDescription_feedback = 4;
      private boolean itemDescriptionFeedback_ ;
      public boolean hasItemDescriptionFeedback() {
        return ((bitField0_ & 0x00000008) == 0x00000008);
      }
      public boolean getItemDescriptionFeedback() {
        return itemDescriptionFeedback_;
      }
      public Builder setItemDescriptionFeedback(boolean value) {
        bitField0_ |= 0x00000008;
        itemDescriptionFeedback_ = value;
        onChanged();
        return this;
      }
      public Builder clearItemDescriptionFeedback() {
        bitField0_ = (bitField0_ & ~0x00000008);
        itemDescriptionFeedback_ = false;
        onChanged();
        return this;
      }
      
      // optional bool item_feedback = 5;
      private boolean itemFeedback_ ;
      public boolean hasItemFeedback() {
        return ((bitField0_ & 0x00000010) == 0x00000010);
      }
      public boolean getItemFeedback() {
        return itemFeedback_;
      }
      public Builder setItemFeedback(boolean value) {
        bitField0_ |= 0x00000010;
        itemFeedback_ = value;
        onChanged();
        return this;
      }
      public Builder clearItemFeedback() {
        bitField0_ = (bitField0_ & ~0x00000010);
        itemFeedback_ = false;
        onChanged();
        return this;
      }
      
      // optional bool deliverTo_feedback = 6;
      private boolean deliverToFeedback_ ;
      public boolean hasDeliverToFeedback() {
        return ((bitField0_ & 0x00000020) == 0x00000020);
      }
      public boolean getDeliverToFeedback() {
        return deliverToFeedback_;
      }
      public Builder setDeliverToFeedback(boolean value) {
        bitField0_ |= 0x00000020;
        deliverToFeedback_ = value;
        onChanged();
        return this;
      }
      public Builder clearDeliverToFeedback() {
        bitField0_ = (bitField0_ & ~0x00000020);
        deliverToFeedback_ = false;
        onChanged();
        return this;
      }
      
      // optional bool price_feedback = 7;
      private boolean priceFeedback_ ;
      public boolean hasPriceFeedback() {
        return ((bitField0_ & 0x00000040) == 0x00000040);
      }
      public boolean getPriceFeedback() {
        return priceFeedback_;
      }
      public Builder setPriceFeedback(boolean value) {
        bitField0_ |= 0x00000040;
        priceFeedback_ = value;
        onChanged();
        return this;
      }
      public Builder clearPriceFeedback() {
        bitField0_ = (bitField0_ & ~0x00000040);
        priceFeedback_ = false;
        onChanged();
        return this;
      }
      
      // optional bool privacy_feedback = 8;
      private boolean privacyFeedback_ ;
      public boolean hasPrivacyFeedback() {
        return ((bitField0_ & 0x00000080) == 0x00000080);
      }
      public boolean getPrivacyFeedback() {
        return privacyFeedback_;
      }
      public Builder setPrivacyFeedback(boolean value) {
        bitField0_ |= 0x00000080;
        privacyFeedback_ = value;
        onChanged();
        return this;
      }
      public Builder clearPrivacyFeedback() {
        bitField0_ = (bitField0_ & ~0x00000080);
        privacyFeedback_ = false;
        onChanged();
        return this;
      }
      
      // optional bool time_feedback = 9;
      private boolean timeFeedback_ ;
      public boolean hasTimeFeedback() {
        return ((bitField0_ & 0x00000100) == 0x00000100);
      }
      public boolean getTimeFeedback() {
        return timeFeedback_;
      }
      public Builder setTimeFeedback(boolean value) {
        bitField0_ |= 0x00000100;
        timeFeedback_ = value;
        onChanged();
        return this;
      }
      public Builder clearTimeFeedback() {
        bitField0_ = (bitField0_ & ~0x00000100);
        timeFeedback_ = false;
        onChanged();
        return this;
      }
      
      // optional bool vendor_feedback = 10;
      private boolean vendorFeedback_ ;
      public boolean hasVendorFeedback() {
        return ((bitField0_ & 0x00000200) == 0x00000200);
      }
      public boolean getVendorFeedback() {
        return vendorFeedback_;
      }
      public Builder setVendorFeedback(boolean value) {
        bitField0_ |= 0x00000200;
        vendorFeedback_ = value;
        onChanged();
        return this;
      }
      public Builder clearVendorFeedback() {
        bitField0_ = (bitField0_ & ~0x00000200);
        vendorFeedback_ = false;
        onChanged();
        return this;
      }
      
      // required string hash_code = 11;
      private java.lang.Object hashCode_ = "";
      public boolean hasHashCode() {
        return ((bitField0_ & 0x00000400) == 0x00000400);
      }
      public String getHashCode() {
        java.lang.Object ref = hashCode_;
        if (!(ref instanceof String)) {
          String s = ((com.google.protobuf.ByteString) ref).toStringUtf8();
          hashCode_ = s;
          return s;
        } else {
          return (String) ref;
        }
      }
      public Builder setHashCode(String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  bitField0_ |= 0x00000400;
        hashCode_ = value;
        onChanged();
        return this;
      }
      public Builder clearHashCode() {
        bitField0_ = (bitField0_ & ~0x00000400);
        hashCode_ = getDefaultInstance().getHashCode();
        onChanged();
        return this;
      }
      void setHashCode(com.google.protobuf.ByteString value) {
        bitField0_ |= 0x00000400;
        hashCode_ = value;
        onChanged();
      }
      
      // @@protoc_insertion_point(builder_scope:clark_pb.Feedback)
    }
    
    static {
      defaultInstance = new Feedback(true);
      defaultInstance.initFields();
    }
    
    // @@protoc_insertion_point(class_scope:clark_pb.Feedback)
  }
  
  private static com.google.protobuf.Descriptors.Descriptor
    internal_static_clark_pb_Feedback_descriptor;
  private static
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_clark_pb_Feedback_fieldAccessorTable;
  
  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\016feedback.proto\022\010clark_pb\"\300\003\n\010Feedback\022" +
      "\024\n\014satisfaction\030\001 \001(\002\022L\n\rattemptResult\030\002" +
      " \001(\0162 .clark_pb.Feedback.AttemptResult:\023" +
      "SUCCESS_NO_FEEDBACK\022\027\n\017action_feedback\030\003" +
      " \001(\010\022 \n\030itemDescription_feedback\030\004 \001(\010\022\025" +
      "\n\ritem_feedback\030\005 \001(\010\022\032\n\022deliverTo_feedb" +
      "ack\030\006 \001(\010\022\026\n\016price_feedback\030\007 \001(\010\022\030\n\020pri" +
      "vacy_feedback\030\010 \001(\010\022\025\n\rtime_feedback\030\t \001" +
      "(\010\022\027\n\017vendor_feedback\030\n \001(\010\022\021\n\thash_code" +
      "\030\013 \002(\t\"m\n\rAttemptResult\022\013\n\007SUCCESS\020\000\022\027\n\023",
      "SUCCESS_NO_FEEDBACK\020\001\022\036\n\032SUCCESS_NO_DETA" +
      "IL_FEEDBACK\020\002\022\026\n\022FAILURE_NO_RESPONE\020\003B\"\n" +
      "\020protos.smartdataB\016FeedbackProtos"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
      new com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner() {
        public com.google.protobuf.ExtensionRegistry assignDescriptors(
            com.google.protobuf.Descriptors.FileDescriptor root) {
          descriptor = root;
          internal_static_clark_pb_Feedback_descriptor =
            getDescriptor().getMessageTypes().get(0);
          internal_static_clark_pb_Feedback_fieldAccessorTable = new
            com.google.protobuf.GeneratedMessage.FieldAccessorTable(
              internal_static_clark_pb_Feedback_descriptor,
              new java.lang.String[] { "Satisfaction", "AttemptResult", "ActionFeedback", "ItemDescriptionFeedback", "ItemFeedback", "DeliverToFeedback", "PriceFeedback", "PrivacyFeedback", "TimeFeedback", "VendorFeedback", "HashCode", },
              protos.smartdata.FeedbackProtos.Feedback.class,
              protos.smartdata.FeedbackProtos.Feedback.Builder.class);
          return null;
        }
      };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }
  
  // @@protoc_insertion_point(outer_class_scope)
}
